#include "communicationwindow.h"
#include "ui_communicationwindow.h"

CommunicationWindow::CommunicationWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::CommunicationWindow) {
    ui->setupUi(this);

    QPalette palette = ui->lblStatus->palette();
    palette.setColor(ui->lblStatus->foregroundRole(), Qt::red);
    ui->lblStatus->setPalette(palette);

    ui->btnSend->setDisabled(true);

    ui->txtIp->setText("127.0.0.1");
    ui->txtPort->setText("20001");

    connect(ui->btnConnect, SIGNAL(released()), this, SLOT(connect_to_server()));
    connect(ui->btnSend, SIGNAL(released()), this, SLOT(slot_send_message()));

    rsa r;
    m_rsa = r;

    m_rsa.generate_keys(m_pub_key, m_priv_key);

    log("Klíče vygenerovány");
    log("Veřejný klíč: n=" + std::to_string(m_pub_key.n) + ", e=" + std::to_string(m_pub_key.e));
    log("Privátní klíč: n=" + std::to_string(m_priv_key.n) + ", d=" + std::to_string(m_priv_key.d));
}

CommunicationWindow::~CommunicationWindow() {
    delete ui;
}

void CommunicationWindow::connect_to_server() {
    if ((m_host = gethostbyname(ui->txtIp->text().toStdString().c_str())) == NULL) {
        error("Špatná adresa");
        return;
    }

    if ((m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
        error("Nelze vytvořit soket");
        return;
    }

    m_addr.sin_family = AF_INET;
    m_addr.sin_port = htons(ui->txtPort->text().toInt(nullptr, 10));
    memcpy(&(m_addr.sin_addr), m_host->h_addr, m_host->h_length);

    if (::connect(m_socket, (sockaddr *) &m_addr, sizeof(m_addr)) == -1) {
        error("Nelze navázat spojení");
        return;
    }

    QPalette palette = ui->lblStatus->palette();
    palette.setColor(ui->lblStatus->foregroundRole(), Qt::green);
    ui->lblStatus->setPalette(palette);

    ui->lblStatus->setText("Připojeno k serveru.");

    ui->btnSend->setEnabled(true);

    send_message(m_socket, "add-key:" + std::to_string(m_pub_key.n) + ":" + std::to_string(m_pub_key.e) + ";");

    ClientListener *cl = new ClientListener(m_socket);
    connect(cl, SIGNAL(message_received(QString)), this, SLOT(received_slot(QString)));
    cl->start();
}

void CommunicationWindow::error(std::string message) {
    ui->txtLog->appendPlainText(("[ERROR]: " + message).c_str());
}

void CommunicationWindow::log(std::string message) {
    ui->txtLog->appendPlainText(("[INFO]: " + message).c_str());
}

int CommunicationWindow::send_message(int socket, std::string message) {
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    const char *msgChar = message.c_str();
    int result = send(socket, (void *) msgChar, message.length(), 0);
    return result;
}

void CommunicationWindow::received_slot(QString message) {
    ui->txtLog->appendPlainText("[INFO]: Přijata zpráva: " + message);
    std::vector<std::string> split = util::split(message.toStdString(), ":");

    if (split[0] == "add-key") {
        m_serv_pub_key.n = atoi(split[1].c_str());
        m_serv_pub_key.e = atoi(split[2].c_str());
        return;
    }

    if (split[0] == "msg") {
        std::string decrypted = m_rsa.decrypt(split[1], m_priv_key);
        log("Dešifrovaná zpráva: "  + decrypted);
        return;
    }
}

void CommunicationWindow::slot_send_message(){
    if(ui->txtMessage->toPlainText().toStdString() != "") {
        std::string encrypted = m_rsa.encrypt(ui->txtMessage->toPlainText().toStdString(), m_serv_pub_key);
        log("Odesílám zprávu: msg:" + encrypted);
        send_message(m_socket, "msg:" + encrypted + ";");
        ui->txtMessage->setPlainText("");
    }
}


