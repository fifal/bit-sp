/********************************************************************************
** Form generated from reading UI file 'communicationwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMMUNICATIONWINDOW_H
#define UI_COMMUNICATIONWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CommunicationWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *txtIp;
    QLineEdit *txtPort;
    QPushButton *btnConnect;
    QLabel *lblStatus;
    QPlainTextEdit *txtMessage;
    QLabel *label;
    QPushButton *btnSend;
    QPlainTextEdit *txtLog;
    QLabel *label_2;
    QFrame *line;

    void setupUi(QMainWindow *CommunicationWindow)
    {
        if (CommunicationWindow->objectName().isEmpty())
            CommunicationWindow->setObjectName(QStringLiteral("CommunicationWindow"));
        CommunicationWindow->resize(755, 331);
        CommunicationWindow->setMinimumSize(QSize(755, 331));
        CommunicationWindow->setMaximumSize(QSize(755, 331));
        centralWidget = new QWidget(CommunicationWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        txtIp = new QLineEdit(centralWidget);
        txtIp->setObjectName(QStringLiteral("txtIp"));
        txtIp->setGeometry(QRect(10, 10, 181, 26));
        txtPort = new QLineEdit(centralWidget);
        txtPort->setObjectName(QStringLiteral("txtPort"));
        txtPort->setGeometry(QRect(200, 10, 113, 26));
        btnConnect = new QPushButton(centralWidget);
        btnConnect->setObjectName(QStringLiteral("btnConnect"));
        btnConnect->setGeometry(QRect(320, 10, 71, 26));
        lblStatus = new QLabel(centralWidget);
        lblStatus->setObjectName(QStringLiteral("lblStatus"));
        lblStatus->setGeometry(QRect(10, 40, 171, 18));
        lblStatus->setAutoFillBackground(false);
        txtMessage = new QPlainTextEdit(centralWidget);
        txtMessage->setObjectName(QStringLiteral("txtMessage"));
        txtMessage->setGeometry(QRect(10, 100, 301, 181));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 80, 121, 16));
        btnSend = new QPushButton(centralWidget);
        btnSend->setObjectName(QStringLiteral("btnSend"));
        btnSend->setGeometry(QRect(10, 290, 301, 26));
        txtLog = new QPlainTextEdit(centralWidget);
        txtLog->setObjectName(QStringLiteral("txtLog"));
        txtLog->setGeometry(QRect(330, 100, 411, 211));
        txtLog->setReadOnly(true);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(330, 80, 121, 16));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(310, 100, 21, 211));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        CommunicationWindow->setCentralWidget(centralWidget);

        retranslateUi(CommunicationWindow);

        QMetaObject::connectSlotsByName(CommunicationWindow);
    } // setupUi

    void retranslateUi(QMainWindow *CommunicationWindow)
    {
        CommunicationWindow->setWindowTitle(QApplication::translate("CommunicationWindow", "BIT - Komunikace", Q_NULLPTR));
        txtIp->setPlaceholderText(QApplication::translate("CommunicationWindow", "IP adresa", Q_NULLPTR));
        txtPort->setPlaceholderText(QApplication::translate("CommunicationWindow", "Port", Q_NULLPTR));
        btnConnect->setText(QApplication::translate("CommunicationWindow", "P\305\231ipojit", Q_NULLPTR));
        lblStatus->setText(QApplication::translate("CommunicationWindow", "Nep\305\231ipojeno k serveru!", Q_NULLPTR));
        label->setText(QApplication::translate("CommunicationWindow", "Zpr\303\241va pro server:", Q_NULLPTR));
        btnSend->setText(QApplication::translate("CommunicationWindow", "Odeslat zpr\303\241vu", Q_NULLPTR));
        label_2->setText(QApplication::translate("CommunicationWindow", "Logger:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CommunicationWindow: public Ui_CommunicationWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMMUNICATIONWINDOW_H
