/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTextEdit *log;
    QPushButton *btnGenerateKeys;
    QTextEdit *txtMsgToEncrypt;
    QLabel *label;
    QLabel *label_2;
    QTextEdit *txtMsgToDecrypt;
    QTextEdit *txtMsgEncrypted;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *lineNumberN;
    QLineEdit *lineNumberD;
    QPushButton *btnDecrypt;
    QTextEdit *txtMsgDecrypted;
    QLabel *label_5;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(853, 651);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(853, 651));
        MainWindow->setMaximumSize(QSize(853, 651));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        log = new QTextEdit(centralWidget);
        log->setObjectName(QStringLiteral("log"));
        log->setEnabled(true);
        log->setGeometry(QRect(10, 450, 831, 191));
        log->setReadOnly(true);
        btnGenerateKeys = new QPushButton(centralWidget);
        btnGenerateKeys->setObjectName(QStringLiteral("btnGenerateKeys"));
        btnGenerateKeys->setGeometry(QRect(10, 180, 401, 26));
        txtMsgToEncrypt = new QTextEdit(centralWidget);
        txtMsgToEncrypt->setObjectName(QStringLiteral("txtMsgToEncrypt"));
        txtMsgToEncrypt->setGeometry(QRect(10, 30, 401, 141));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 141, 18));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 430, 59, 18));
        txtMsgToDecrypt = new QTextEdit(centralWidget);
        txtMsgToDecrypt->setObjectName(QStringLiteral("txtMsgToDecrypt"));
        txtMsgToDecrypt->setGeometry(QRect(430, 30, 401, 141));
        txtMsgEncrypted = new QTextEdit(centralWidget);
        txtMsgEncrypted->setObjectName(QStringLiteral("txtMsgEncrypted"));
        txtMsgEncrypted->setGeometry(QRect(10, 260, 401, 141));
        txtMsgEncrypted->setReadOnly(true);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 240, 121, 18));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(430, 10, 141, 18));
        lineNumberN = new QLineEdit(centralWidget);
        lineNumberN->setObjectName(QStringLiteral("lineNumberN"));
        lineNumberN->setGeometry(QRect(430, 180, 191, 26));
        lineNumberD = new QLineEdit(centralWidget);
        lineNumberD->setObjectName(QStringLiteral("lineNumberD"));
        lineNumberD->setGeometry(QRect(640, 180, 191, 26));
        btnDecrypt = new QPushButton(centralWidget);
        btnDecrypt->setObjectName(QStringLiteral("btnDecrypt"));
        btnDecrypt->setGeometry(QRect(430, 210, 401, 26));
        txtMsgDecrypted = new QTextEdit(centralWidget);
        txtMsgDecrypted->setObjectName(QStringLiteral("txtMsgDecrypted"));
        txtMsgDecrypted->setGeometry(QRect(430, 260, 401, 141));
        txtMsgDecrypted->setReadOnly(true);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(430, 240, 121, 18));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Qt RSA - BIT, Filip Jani - A14B0269P ", Q_NULLPTR));
        btnGenerateKeys->setText(QApplication::translate("MainWindow", "Vygenerovat kl\303\255\304\215e a za\305\241ifrovat", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Zpr\303\241va k za\305\241ifrov\303\241n\303\255", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Log", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Za\305\241ifrovan\303\241 zpr\303\241va", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Zpr\303\241va k de\305\241ifrov\303\241n\303\255", Q_NULLPTR));
        lineNumberN->setPlaceholderText(QApplication::translate("MainWindow", "\304\214\303\255slo n priv\303\241tn\303\255ho kl\303\255\304\215e", Q_NULLPTR));
        lineNumberD->setPlaceholderText(QApplication::translate("MainWindow", "\304\214\303\255slo d priv\303\241tn\303\255ho kl\303\255\304\215e", Q_NULLPTR));
        btnDecrypt->setText(QApplication::translate("MainWindow", "De\305\241ifrovat", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "De\305\241ifrovan\303\241 zpr\303\241va", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
