//
// Created by fifal on 13.4.17.
//

#ifndef BIT_SP_NETWORK_H
#define BIT_SP_NETWORK_H

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
class network {
public:
    static std::string receive_message(int socket);
};


#endif //BIT_SP_NETWORK_H
