#ifndef COMMUNICATIONWINDOW_H
#define COMMUNICATIONWINDOW_H

#include <QMainWindow>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>
#include "rsa.h"
#include <QThread>
#include "ClientListener.h"

namespace Ui {
    class CommunicationWindow;
}
class CommunicationWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CommunicationWindow(QWidget *parent = 0);
    std::string receive_message(int socket);
    ~CommunicationWindow();

private:
    Ui::CommunicationWindow *ui;

    rsa::private_key m_priv_key;
    rsa::public_key m_pub_key;

    rsa::public_key m_serv_pub_key;
    rsa m_rsa;

    int m_socket;
    struct sockaddr_in m_addr;
    hostent *m_host;

    int send_message(int socket, std::string message);
    void error(std::string message);
    void log(std::string message);

private slots:
    void connect_to_server();
    void received_slot(QString message);
    void slot_send_message();
};


#endif // COMMUNICATIONWINDOW_H
