//
// Created by fifal on 13.4.17.
//

#include "network.h"

std::string network::receive_message(int socket) {
    int max_len = 20000;
    // Initialization of char array with max length defined in server configuration
    char message[max_len];
    // Setting all chars in array to 0
    memset(message, 0, max_len);

    // Reading message from client
    int result = (int) read(socket, &message, max_len - 1);

    std::string message_str;

    // If result is less than zero -> error while getting message
    if (result < 0) {
        //error("Chyba při příjmání zprávy.");
        return "";
    }

        // If result equals to zero -> client dropped -> closes client's socket
    else if (result == 0) {
        //error("Ztraceno spojení s klientem, končím aplikaci.");
        shutdown(socket, SHUT_RDWR);
        //log("--- Končím server ---");
        exit(1);
    }

        // Else message handling
    else {
        int i = 0;

        // While char at index i is not ';' add char into messageString
        while (message[i] != ';' && i < max_len && message[i] != '\0') {
            message_str += message[i];
            i++;
        }
        if (message[i] == ';') {
            message_str += message[i];
        }

        return message_str;
    }

}