#include "mainwindow.h"
#include "communicationwindow.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    if (argc == 2 && strcmp(argv[1], "-c") == 0) {
        QApplication a(argc, argv);
        MainWindow w;
        w.show();
        return a.exec();
    } else if (argc == 2 && strcmp(argv[1], "-s") == 0) {
        QApplication a(argc, argv);
        CommunicationWindow w;
        w.show();
        return a.exec();
    } else {
        std::cerr
                << "Špatně zadané spouštěcí parametry. Použití:\n\t./BIT_sp -c\t pro aplikaci "
                << "bez TCP komunikace.\n\t./BIT_sp -s\t pro aplikaci s TCP komunikací"
                << std::endl;
    }
}
