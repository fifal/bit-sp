//
// Created by fifal on 13.4.17.
//

#include "ClientListener.h"

ClientListener::ClientListener(int socket) {
    m_socket = socket;
}

void ClientListener::run() {
    //std::cout << "[INFO] Vlákno pro listener spuštěno" << std::endl;
    while (true) {
        std::string message = network::receive_message(m_socket);
        //std::cout << "[INFO] Přijata zpráva: " << message << std::endl;
        message.at(message.length() - 1) = ' ';
        emit message_received((QString) message.c_str());
    }
}